//
//  StoreCastTaskUITests.swift
//  StoreCastTaskUITests
//
//  Created by Arup Paul on 13/02/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import XCTest

class StoreCastTaskUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /*func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        let cellsQuery = app.collectionViews.cells
        cellsQuery.otherElements.containing(.staticText, identifier:"Scientists recording data in laboratory").children(matching: .image).element.swipeUp()
        cellsQuery.otherElements.containing(.staticText, identifier:"Science lab").children(matching: .image).element.swipeUp()
        
        
        
    }*/
    
    func testSearchBarShow(){
        let app = XCUIApplication()
        let searchButton = app.navigationBars["StoreCastTask.View"].buttons["ic search white 48pt"]
        searchButton.tap()
        XCTAssert(app.keyboards.count > 0)
    }
    
    func testSearchBarHide(){
        let app = XCUIApplication()
        let searchButton = app.navigationBars["StoreCastTask.View"].buttons["ic search white 48pt"]
        searchButton.tap()
        searchButton.tap()
        XCTAssert(app.keyboards.count == 0)
    }
    
    func testSearchTextEnty(){
        let app = XCUIApplication()
        let searchButton = app.navigationBars["StoreCastTask.View"].buttons["ic search white 48pt"]
        searchButton.tap()
        let searcText = app.searchFields.element
        searcText.typeText("Messi")
        XCTAssert(searcText.value as? String == "Messi")
        
    }
    
    func testSearch(){
        
        let app = XCUIApplication()
        let searchinfoStaticText = app.staticTexts["searchInfo"]
        let oldText = searchinfoStaticText.label
        
        app.navigationBars["StoreCastTask.View"].buttons["ic search white 48pt"].tap()
        app.searchFields.element.typeText("Messi")
        app.buttons["Search"].tap()
        //app.typeText("\n")
        
        let newText = searchinfoStaticText.label
        
        XCTAssertTrue(oldText != newText)

        
    }
    
}
