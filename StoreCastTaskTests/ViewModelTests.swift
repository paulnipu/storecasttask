//
//  ViewModelTests.swift
//  StoreCastTask
//
//  Created by Arup Paul on 10/02/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import XCTest
import RxSwift
@testable import StoreCastTask

class ViewModelTests: XCTestCase {
    
    let disposeBag = DisposeBag.init()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testLoadDataByPage(){
        let viewModel = ViewModel()
        let asyncExpectation = expectation(description: "loadimage by page")
        var count = 0;
        viewModel.collectionData.asObservable().subscribe{
            count = $0.element?.count ?? 0
            }.addDisposableTo(disposeBag)
        
        DispatchQueue.global().async {
            repeat{
                
                viewModel.loadNextData(phrase: "test")
                
            }while(count < 100)
            asyncExpectation.fulfill()
        }
        self.waitForExpectations(timeout: 6, handler: {_ in
            XCTAssert(count >= 100)
        })
    }
    
}
