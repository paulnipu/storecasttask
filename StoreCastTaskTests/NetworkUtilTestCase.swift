//
//  NetworkUtilTestCase.swift
//  StoreCastTask
//
//  Created by Arup Paul on 10/02/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import XCTest
import RxSwift
@testable import StoreCastTask

class NetworkUtilTestCase: XCTestCase {
    
    let disposeBag = DisposeBag.init()
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFetchImageSuccess() {
        let asyncExpectations = expectation(description: "fetchimagesuccess")
        var data : PageDataModel?
        let url = CustomUrl(page: 1, page_size: 1, phrase: "test", baseUrl: NetworkUtil.sharedInstance.baseUrl)
        NetworkUtil.sharedInstance.fetchImage(url: url, header: ["Api-Key" : "4x3mqfykgft2uj2zynnw4b9w"]).subscribe(onNext: { (images : PageDataModel) -> () in
            data = images
            asyncExpectations.fulfill()
        }, onError: { (error) in
            asyncExpectations.fulfill()
        }, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
        
        self.waitForExpectations(timeout: 5, handler: {_ in
            
            XCTAssert(data != nil)
        })
    }
    
    func testFetchImageFailed() {
        let asyncExpectations = expectation(description: "fetchimagesuccess")
        var data : Error?
        let url = CustomUrl(page: 1, page_size: 1, phrase: "test", baseUrl: "https://api.gettyimages.com/v3/search/images/wrong?")
        NetworkUtil.sharedInstance.fetchImage(url: url, header: ["Api-Key" : "4x3mqfykgft2uj2zynnw4b9w"]).subscribe(onNext: { (images : PageDataModel) -> () in

            asyncExpectations.fulfill()
        }, onError: { (error) in
            data = error
            asyncExpectations.fulfill()
        }, onCompleted: nil, onDisposed: nil).addDisposableTo(disposeBag)
        
        self.waitForExpectations(timeout: 5, handler: {_ in
            
            XCTAssert(data != nil)
        })
    }
    
}
