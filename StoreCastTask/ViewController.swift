//
//  ViewController.swift
//  StoreCastTask
//
//  Created by Arup Paul on 10/02/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import TTGSnackbar

class ViewController: UIViewController {
    
    let viewModel = ViewModel()
    
    private let disposeBag = DisposeBag.init()
    
    private let cellIdentifier : String = "imageCell"
    
    private var searchText : String = "test"
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    @IBOutlet weak var searchbar: UISearchBar!
    
    
    @IBOutlet weak var btnSearch: UIBarButtonItem!
    
    
    @IBOutlet weak var searchbarLayoutConstrain: NSLayoutConstraint!
    
    
    @IBOutlet weak var txtInfo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupSearchBar()
        setupCollectionView()
        setupInfo()
        viewModel.loadNextData(phrase: searchText)
    }
    
    private func setupInfo() {
        
        viewModel.searchInfo.asObservable().bindTo(txtInfo.rx.text).addDisposableTo(disposeBag)
        
    }
    
    private func setupSearchBar(){
        btnSearch.rx.tap.bindNext {[unowned self] in
            //self.searchbarLayoutConstrain.constant = self.searchbarLayoutConstrain.constant == 0 ? -44 : 0
            if self.searchbarLayoutConstrain.constant == 0 {
                self.searchbar.endEditing(true)
                self.searchbarLayoutConstrain.constant = -44
            }else {
                self.searchbarLayoutConstrain.constant = 0
                self.searchbar.becomeFirstResponder()
            }
            UIView.animate(withDuration: 0.5, animations: {
                self.view.layoutSubviews()
            }, completion: {[unowned self] (isCompleted) in
                if isCompleted {
                    self.setupCollectionViewLayout()
                }
            })
            }.addDisposableTo(disposeBag)
        
        searchbar.rx.searchButtonClicked.asObservable().subscribe {[unowned self] (event) in
            self.searchText = self.searchbar.text ?? self.searchText
            if self.viewModel.collectionData.value.count > 0 {
                self.collectionView.scrollToItem(at: IndexPath(row: 0, section: 0), at: UICollectionViewScrollPosition.top, animated: true)
            }
            self.searchbar.endEditing(true)
            self.viewModel.loadNextData(phrase: self.searchText)
            }.addDisposableTo(disposeBag)
    }
    
    private func setupCollectionViewLayout(){
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.width - 2) / 2, height: (collectionView.frame.height - 6) / 3.0)
        layout.minimumLineSpacing = 2
        layout.minimumInteritemSpacing = 2
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        collectionView.collectionViewLayout = layout
        self.view.layoutIfNeeded()
    }
    
    private func setupCollectionView(){
        setupCollectionViewLayout()
        viewModel.collectionData.asObservable()
            .bindTo(collectionView.rx.items(cellIdentifier: cellIdentifier, cellType: CollectionViewCell.self))
            {[unowned self]row, data, cell in
                cell.setupCellView(data: data)
                if row == self.viewModel.collectionData.value.count - 1 && !self.viewModel.isLoading{
                    self.viewModel.loadNextData(phrase: self.searchText)
                }
            }.addDisposableTo(disposeBag)
        collectionView.rx.itemSelected.bindNext {[unowned self] indexpath in
            let snackBar = TTGSnackbar(message: self.viewModel.collectionData.value[indexpath.row].title ?? "", duration: .short)
            snackBar.show()
            }.addDisposableTo(disposeBag)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

