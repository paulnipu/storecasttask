//
//  NetworkUtil.swift
//  StoreCastTask
//
//  Created by Arup Paul on 10/02/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Alamofire
import ObjectMapper

enum ErrorType : Error {
    case Error_Parse_Json
    
    case Error_Request_Failed
    
    case Error_Invalid_Url
    
}

struct CustomUrl : URLConvertible {
    let page : Int
    
    let page_size : Int
    
    let phrase : String
    
    let baseUrl : String

    func asURL() throws -> URL {
        guard let url = URL(string: "\(baseUrl)?page=\(page)&page_size=\(page_size)&phrase=\(phrase)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
            else {
                throw ErrorType.Error_Invalid_Url
        }
        return url
    }
}

class NetworkUtil {
    
    static let sharedInstance = NetworkUtil()
    
    let defaultApiHeader = ["Api-Key" : "4x3mqfykgft2uj2zynnw4b9w"]
    
    let baseUrl = "https://api.gettyimages.com:443/v3/search/images"
    
    private init() {
        
    }
    
    func fetchImage<T : Mappable>(url:CustomUrl,header:[String : String],method:HTTPMethod = .get) -> Observable<T> {
        return Observable<T>.create({ (subscriber) -> Disposable in
            Alamofire.request(url, method: method, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: { (response) in
                if response.result.isSuccess {
                    debugPrint("Network call \(url.page)")
                    //Success Response
                    if let dict = response.result.value as? [String : Any] {
                        if let data = Mapper<T>().map(JSON: dict) {
                            subscriber.onNext(data)
                        }else {
                            subscriber.onError(ErrorType.Error_Parse_Json)
                        }
                        
                    }else {
                        subscriber.onError(ErrorType.Error_Parse_Json)
                    }
                    
                }else {
                    // Failed Response
                    debugPrint(response.response.debugDescription)
                    subscriber.onError(ErrorType.Error_Request_Failed)
                }
                
                subscriber.onCompleted()
            })
            return Disposables.create()
        })
    }
}
