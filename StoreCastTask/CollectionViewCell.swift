//
//  CollectionViewCell.swift
//  StoreCastTask
//
//  Created by Arup Paul on 10/02/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import UIKit
import Kingfisher

class CollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    func setupCellView(data : ImageDataModel){
        
        lblTitle.text = data.title ?? ""
        if let url = URL(string: data.uri ?? "") {
            imageView.kf.setImage(with: url)
        }
    }
    
    
}
