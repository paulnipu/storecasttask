//
//  ViewModel.swift
//  StoreCastTask
//
//  Created by Arup Paul on 10/02/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import UIKit
import RxSwift
import ObjectMapper
class ViewModel: NSObject {
    
    var collectionData : Variable<[ImageDataModel]> = Variable([])
    
    var searchInfo : Variable<String> = Variable("")
    
    private var currentTotal : NSNumber = 20 {
        willSet {
            searchInfo.value = "Search By: \"\(currentPhrase)\" (\(newValue))"
        }
    }
    
    private var currentPage : Int = 0
    
    private let pageSize : Int = 20
    
    private var numberOfTempObject = 0
    
    private var currentPhrase : String = "" {
        willSet {
            if currentPhrase != newValue {
                currentPage = 0
                numberOfTempObject = 0
                currentTotal = 20
                //isLoading = false
                searchInfo.value = "Search By: \(currentPhrase) \((currentTotal))"
                collectionData.value.removeAll()
            }
        }
    }
    
    private let disposeBag = DisposeBag.init()
    
    var isLoading : Bool = false
    
    override init() {
        
    }
    
    func loadNextData(phrase : String = "") {
        currentPhrase = phrase
        if currentTotal.intValue > collectionData.value.count && !isLoading{
            currentPage += 1
            
            self.numberOfTempObject = currentTotal.intValue - collectionData.value.count >= pageSize ? pageSize : currentTotal.intValue - collectionData.value.count > 0 ? currentTotal.intValue - collectionData.value.count : 0 //Calculate how many objects i have to load and calculate amount of null object
            let temp  = repeatElement(ImageDataModel(), count: numberOfTempObject)
            collectionData.value.append(contentsOf:temp)
            
            let url = CustomUrl(page: currentPage, page_size: pageSize, phrase: currentPhrase, baseUrl: NetworkUtil.sharedInstance.baseUrl)
            isLoading = true
            NetworkUtil.sharedInstance.fetchImage(url: url, header: NetworkUtil.sharedInstance.defaultApiHeader).subscribe(onNext: { [unowned self] (pageData : PageDataModel) -> () in
                
                if  self.numberOfTempObject >= 0 {
                    
                    self.collectionData.value.replaceSubrange(self.getRangeForReplace(), with: pageData.result)
                    self.isLoading = false
                    self.numberOfTempObject = 0
                }
                
                self.currentTotal = pageData.totalResult
                
                }, onError: {[unowned self] (error) in
                    self.isLoading = false
                    self.searchInfo.value = "Failed to load"
                    debugPrint(error)
            }, onCompleted: {
                self.isLoading = false
            }, onDisposed: nil).addDisposableTo(disposeBag)
        }else{
            if isLoading{
                debugPrint("Another Search is ongoing")
            }else{
                debugPrint("No data to load")
            }
            
        }
        
    }
    
    private func getRangeForReplace() -> ClosedRange<Int> {
        let endRange = collectionData.value.count - 1
        let startRange = endRange - numberOfTempObject  + 1
        
        return ClosedRange.init(uncheckedBounds: (startRange,endRange))
        
    }
}
