//
//  PageDataModel.swift
//  StoreCastTask
//
//  Created by Arup Paul on 10/02/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import UIKit
import ObjectMapper

class PageDataModel:NSObject, Mappable {
    
    var totalResult : NSNumber = 0
    
    var result : [ImageDataModel] = []
    
    override init() {
        
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        totalResult <- map["result_count"]
        
        result <- map["images"]
        
    }
}
