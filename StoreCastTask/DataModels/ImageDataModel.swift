//
//  ImageDataModel.swift
//  StoreCastTask
//
//  Created by Arup Paul on 10/02/17.
//  Copyright © 2017 Arup Paul. All rights reserved.
//

import UIKit
import ObjectMapper

class ImageDataModel:NSObject, Mappable {

    var id : String?
    
    var caption : String?
    
    var uri : String?
    
    var title : String?
    
    override init() {
        super.init()
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        
        caption <- map["caption"]
        
        uri <- map["display_sizes.0.uri"]
        
        title <- map["title"]
        
        uri = uri?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    }
}
